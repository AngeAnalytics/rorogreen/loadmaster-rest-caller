LoadMaster REST caller
======================

Install
-------

* Install Python:
  * Download Python from https://www.python.com/downloads/
  * Run installer, use default options
* Install `requests` module
  * Open Command Prompt
  * Change directory to where Python was installed
  * Run `pip install requests`
* [Download `loadmaster-rest-call.py`](https://gitlab.com/AngeAnalytics/rorogreen/loadmaster-rest-caller/-/blob/master/loadmaster-rest-caller.py)

Run
---

Start LoadMaster GUI

Start LoadMasterAPI as Administrator

Call script with directory as single argument.

Can be done by dragging directory and dropping it on script.

Functionality
-------------

* Get vessel data and save in `vessel.csv`
* Delete all cargo currently loaded in LoadMaster
* Open file `cargo.csv` and load that as cargo in LoadMaster, save load results in `loads.csv`
* If file `tanks.csv` exists open it and use for setting tanks data in LoadMaster, save results in `tanks-loads.csv`
* Get tank status from LoadMaster and save in `tanks-results.csv`
* Get results from LoadMaster and save in `results.csv`
