#!/usr/bin/env python3

base_path = 'http://localhost:5000'
calls_log = None

def get(path):
    print("========================================", file=calls_log)
    print(f"GET {path}", file=calls_log)
    response = requests.get(f"{base_path}/{path}")
    print(response, file=calls_log)
    print(response.text, file=calls_log)
    return response.json()

def post(path, payload):
    print("========================================", file=calls_log)
    print(f"POST {path}", file=calls_log)
    print(f"{payload}", file=calls_log)
    response = requests.post(f"{base_path}/{path}", json=payload)
    print(response, file=calls_log)
    print(response.text, file=calls_log)
    return response.json()

def delete(path):
    print("========================================", file=calls_log)
    print(f"DELETE {path}", file=calls_log)
    response = requests.delete(f"{base_path}/{path}")
    print(response, file=calls_log)
    print(response.text, file=calls_log)
    return response.json()

def vessel():
    print("Get vessel data")
    vessel = get('vessel')
    with open('vessel.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        for k, v in vessel.items():
            writer.writerow([k, v])

def delete_cargo():
    print(f"Will delete ALL cargos")
    delete(f"drycargos/ALL")

def load_cargo():
    with open('cargo.csv', newline='') as cargo_csv, open('loads.csv', 'w', newline='') as loads_csv:
        reader = list(csv.DictReader(cargo_csv))
        print(f"Will load {len(reader)} cargos")
        result = post("drycargos", reader)
        print(f"  {result['msg']}")
        writer = csv.writer(loads_csv)
        writer.writerow(['success', 'msg', 'msgDetailed', 'code'])
        writer.writerow(result.values())

def tanks():
    if Path('tanks.csv').is_file():
        with open('tanks.csv', newline='') as tanks_csv, open('tanks-loads.csv', 'w', newline='') as loads_csv:
            reader = csv.reader(tanks_csv)
            headers = next(reader)
            rows = list(reader)
            print(f"Will load {len(rows)} tanks")
            writer = csv.writer(loads_csv)
            writer.writerow(['success', 'msg', 'msgDetailed', 'code'])
            for row in rows:
                result = post("cargos", dict(zip(headers, row)))
                print(f"  {result['msg']}")
                writer.writerow(result.values())
    else:
        print("Skip setting tanks as no 'tanks.csv' file found")


def tanks_results():
    print("Get tanks data")
    results = get('tanks')
    with open('tanks-results.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['tankIndex', 'maxVolume', 'volume', 'prcVolume',
            'weight', 'density', 'lcg', 'tcg', 'vcg', 'fsm', 'shortTankName',
            'fullTankName', 'shortNameTankGroup', 'fullNameTankGroup',
            'productName', 'colorHex', 'numberOfTankGroup'])
        for i in results['tanks']:
            writer.writerows([i.values()])

def results():
    print("Get results data")
    results = get('results')
    with open('results.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['code', 'unit', 'val', 'status'])
        for i in results['itemList']:
            writer.writerows([i.values()])

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('dir')
    args = parser.parse_args()
    dir = args.dir
    os.chdir(dir)
    global calls_log
    with open('calls.log', 'w') as calls_log:
        vessel()
        delete_cargo()
        load_cargo()
        tanks()
        tanks_results()
        results()

try:
    print("LoadMaster REST Caller")
    import argparse
    import csv
    #from http.client import HTTPConnection
    import os
    from pathlib import Path
    import traceback
    import requests
    import sys
    #sys.argv = ['loadmaster-rest-caller', 'example']  # For debugging in idle
    #HTTPConnection.debuglevel=1
    main()

except:
    print()
    traceback.print_exc()

print()
print()
input("Press Enter to continue... ")
